import { Component, OnInit, Input } from '@angular/core';
import { MIXT } from '../mixtape-mock';

@Component({
  selector: 'app-mixtapes',
  templateUrl: './mixtapes.component.html',
  styleUrls: ['./mixtapes.component.css']
})
export class MixtapesComponent implements OnInit {

  public mixtapeList = MIXT;

  @Input() mixtape;

  ngOnInit() {
  }

}
