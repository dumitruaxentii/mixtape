import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  mixtapeForm: FormGroup;

  public mixtapes = [];

  constructor() {
    this.mixtapeForm = this.createMixtapeForm();
  }

  createMixtapeForm() {
    return new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(4)]),
      cover: new FormControl(),
      youtube: new FormControl()
    });
  }

  onSubmit() {
    if (Object.keys(this.mixtapeForm.value).length === 3) {
      this.mixtapes.push(this.mixtapeForm.value);
    }
  }

  ngOnInit() { }

}
