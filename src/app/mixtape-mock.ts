import { Mixtape } from './mixtape';

export const MIXT: Mixtape[] = [
  {
    'name': 'Central Mobility Strategist',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/tweetubhai/128.jpg'
  },
  {
    'name': 'Principal Brand Agent',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/besbujupi/128.jpg'
  },
  {
    'name': 'Senior Solutions Architect',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/victordeanda/128.jpg'
  },
  {
    'name': 'Chief Usability Technician',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/jydesign/128.jpg'
  },
  {
    'name': 'Future Usability Liaison',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/davidsasda/128.jpg'
  },
  {
    'name': 'Forward Configuration Assistant',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/geshan/128.jpg'
  },
  {
    'name': 'Principal Applications Director',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/salleedesign/128.jpg'
  },
  {
    'name': 'Central Mobility Technician',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/igorgarybaldi/128.jpg'
  },
  {
    'name': 'Central Mobility Planner',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/bryan_topham/128.jpg'
  },
  {
    'name': 'Future Web Facilitator',
    'cover': 'https://s3.amazonaws.com/uifaces/faces/twitter/hgharrygo/128.jpg'
  }
];
