export class Mixtape {

  constructor(
    public name: string,
    public cover: string,
    public youtube?: boolean
  ) {  }
}
