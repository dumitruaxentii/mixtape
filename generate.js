module.exports = function() {
  var faker = require("faker");
  var _ = require ("lodash");
  return {
    mixtape: _.times(10, function (n) {
      return {
        name: faker.name.title(),
        cover: faker.image.avatar()
      }
    })
  }
}